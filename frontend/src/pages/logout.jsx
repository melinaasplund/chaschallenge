import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { UserAuth } from '../../context/AuthContext'
import { useState } from 'react'
import Modal from '../components/modal'

const Logout = () => {
	const [isOpen, setIsOpen] = useState(false)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [error, setError] = useState('')
	const [isModalOpen, setIsModalOpen] = useState(false)
	const { user, logOut, logIn } = UserAuth()
	const router = useRouter()
	//console.log(user.email)

	const handleLogout = async () => {
		try {
			await logOut()
			console.log('logged out', user?.name)
			// router.push('/')
			// alert('Du har blivit utloggad')
			setIsModalOpen(!isModalOpen)
		} catch (error) {
			console.log(error)
			setError(error)
		}
	}

	const handleLogin = async (e) => {
		setError('')
		e.preventDefault()
		try {
			await logIn(email, password)
			router.push('/')
		} catch (error) {
			console.log(error)
			setError('Inloggningen misslyckades! Har du angett rätt e-postadress och lösenord? Observera att endast registrerade medlemmar kan logga in.')
			router.push('/')
		}
	}

	const handleModalClose = async () => {
		setIsModalOpen(!isModalOpen)
		router.push('/')
	}

	return (
		<div className='flex items-center justify-between z-[100] w-full'>
			{user ? (
				<div>
					<button
						onClick={handleLogout}
						className='bg-[#026E78] w-[343px] h-[48px] py-[12px] px-[20px] text-lg flex-none order-none grow  not-italic leading-[100%] text-[16px] rounded-full mt-2 shadow-lg font-semibold shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da]'>
						Logga ut
					</button>
				</div>
			) : (
				<div>
					<button
						onClick={() => setIsOpen(!isOpen)}
						className='bg-[#E7A422] w-auto h-[44px] py-[10px] px-[18px] flex-none order-none grow-0 font-normal not-italic leading-[100%]  text-[16px] rounded-full mt-2 border-[#E7A422] shadow-[0_1pxbg-[#E7A422] border_2px_rgba(198,228,246,0.05)] text-white hover:bg-gradient-to-br hover:from-[rgba(207,147,30)]  hover:to-[rgba(0,0,0,0.1)] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05)] hover:border hover:border-[rgba(186,132,27)] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focw-[343px]  focus:ring-[#94f3da]'>
						{isOpen ? 'Stäng' : 'Logga in'}
					</button>
					{isOpen && (
						<div className='max-w-[400px] bg-[#E7A422] pt-8 pb-2 pr-4 pl-4 mt-5 mb-10 rounded'>
							<div className='mx-auto'>
								<h1 className='text-center  text-[#3B4256] font-medium text-2xl'>Logga in</h1>
								{error ? <p className='p-3 text-[#3B4256] font-medium  my-2 text-xs'>{error}</p> : null}
								<div className='w-full flex flex-col py-4'>
									<input
										onChange={(e) => setEmail(e.target.value)}
										className='rounded p-2 my-2'
										type='email'
										placeholder='E-postadress*'
										autoComplete='email'
									/>
									<input
										onChange={(e) => setPassword(e.target.value)}
										className='rounded p-2 my-2'
										type='password'
										placeholder='Lösenord*'
										autoComplete='current-password'
									/>
									<button
										onClick={handleLogin}
										className=' rounded-full bg-[#026E78] py-[10px] px-[16px] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da] w-32 text-sm mt-2 font-normal not-italic leading-[100%]  text-[14px]'>
										Logga in
									</button>

									<p className='pt-8 pb-3'>
										<span className=' text-[#3B4256] font-medium text-lg'>Har du inget konto än?</span>{' '}
										<Link href='/signup' className='underline  text-[#3B4256] font-medium text-lg'>
											Bli medlem
										</Link>
									</p>
								</div>
								<p className='pt-4 text-xs'>* = obligatoriskt fält</p>
							</div>
						</div>
					)}
				</div>
			)}
			{isModalOpen ? (
				<Modal>
					<div className='flex flex-col gap-4 bg-white px-8 py-8 rounded-lg'>
						<h2 className='text-md mt-2 font-semibold pr-48'>Du har blivit utloggad!</h2>

						<div className='flex flex-row'>
							<button
								onClick={handleModalClose}
								className='rounded-full bg-[#026E78] py-[10px] px-[16px] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da] w-32 text-sm mt-2 font-normal not-italic leading-[100%]  text-[14px]'>
								Stäng
							</button>
						</div>
					</div>
				</Modal>
			) : null}
		</div>
	)
}

export default Logout
