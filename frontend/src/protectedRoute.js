import React from 'react'
import { useRouter } from 'next/router'
import { UserAuth } from '../context/AuthContext'

export function withPublic(Component) {
	return function WithPublic(props) {
		const auth = UserAuth()
		const router = useRouter()

		if (auth.user) {
			router.push('/account')
			return <h1>Loading...</h1>
		}
		return <Component auth={auth} {...props} />
	}
}

export function withProtected(Component) {
	return function WithProtected(props) {
		const auth = UserAuth()
		const router = useRouter()

		console.log('detta är auth.user.uid', auth.user?.uid)

		if (auth.user?.uid !== '8CvehFe67jcQUsINJKV477GDo7A3') {
			typeof window !== 'undefined' && router.push('/unauthorized')
			return <h1>Loading...</h1>
		}

		return <Component auth={auth} {...props} />
	}
}
