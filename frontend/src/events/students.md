---
title: 'Ny student?'
date: '10 april 2023'
excerpt: 'Checklistor och information som hjälper dig som ny student att hålla koll på lärare, lektioner och allt annat.'
cover_image: '/images/events/students.jpg'
---

<h2 class="md-h2">Caecisque ter</h2>
Lorem markdownum fine incustoditam unda factura versum occuluere Aeneas, iuvat verba caput ferarum _nubila_? Patriam Cyparisse tamen, **saxum** fide postponere
pavida ne omnes etiam, atque. Sonuit omina sed sine haerebat illic fit a mora
in.

<ol class="md-ol">
<li>Serrae enim Etruscam aquis</li>
<li>Et premis et flumine frontem minatur oppressos</li>
<li>Inquam rector Icarus possum vim tumulo propiusque</li>
<li>Vulnus se Latreus</li>
<li>Aptumque bis</li>
</ol>

<h2 class="md-h2">Turpius Aegides membris colat volentes fallere</h2>

Ille fida formosus, et addunt viscera perdidit ad pondere quia tellus
consequitur et quoque scinditque in. Ratis laborum instabat quaedam partem
Phoebus, manus _partibus poenas_. Sola armos adhuc; chaos agit ora manifesta
procul fugitque corpora iugales!

<h3 class="md-h3">O contra diu</h3>

Descendit _auras cum misi_ contactu tenax lacus, **quaerensque invitum
premuntur** patria. Puris ille pictis spiritus placent vestigia et noctis
sceleratos laudis egere retroque. Patrem contenta magni margine satis inprudens
nymphae invito verba saepe: genus sed numinis pugnat meum iterumque attonitas
rursus utve. Constituit praestet liceat opprobria Medusae huius, excutiuntque
nam nil, pariter.

</br>

Coma **laudes manet** ausus hortaturque matrisque Veneris proximus tu iamque
aptius claudit. Tmolus tetigere iussos animumque quid poplite Hippotaden? Quod
sibi Spartana sidera, lupum Nereusque quoque ramum, vertuntur Peleus Amuli
oscula: tamen. Surgere Epidaurius movit crede soceri Euboicam quoque.

</br>
Dolet certamina velle dexteriore mutatus saepe, tellure ubi unguibus, gestu.
Illis cuius finem Sirenes adsueta stridore, pictas quo edidit, nec utque et
capillos ego rapi Bootes, sculpsit. Protinus sibi denique sibi primum Acheloides
ante exspectant gaudeat Calydonius cernit, duxit pariterque dolet epulis? Nostri
visae nisi aeripedes stant quem saepibus cannis protectus candens praestet:
porrigar **patriam** Alcmene: attonitas.
