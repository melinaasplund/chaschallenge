---
title: 'Harmonisk, kreativ och effektiv arbetsmiljö'
date: '30 april 2023'
excerpt: 'Den fysiska miljön har betydelse för hur väl vi presterar.'
cover_image: '/images/events/computer.jpg'
---

Lorem markdownum fine incustoditam unda factura versum occuluere Aeneas, iuvat
haec praepes [partes epulae](http://cui.com/), in egisse de. Caecisque ter
manus. Munere in exhalat, ferre sed [habe quaeque saepe](http://ne.org/fretum)
verba caput ferarum _nubila_? Patriam Cyparisse tamen, **saxum** fide postponere
pavida ne omnes etiam, atque. Sonuit omina sed sine haerebat illic fit a mora
in.

<ol class="md-ol">
<li>Serrae enim Etruscam aquis</li>
<li>Et premis et flumine frontem minatur oppressos</li>
<li>Inquam rector Icarus possum vim tumulo propiusque</li>
<li>Vulnus se Latreus</li>
<li>Aptumque bis</li>
</ol>

<h2 class="md-h2">Turpius Aegides membris colat volentes fallere</h2>

Ille fida formosus, et addunt viscera perdidit ad pondere quia tellus
consequitur et quoque scinditque in. Ratis laborum instabat quaedam partem
Phoebus, manus _partibus poenas_. Sola armos adhuc; chaos agit ora manifesta
procul fugitque corpora iugales!

<h2 class="md-h2">O contra diu</h2>

Descendit _auras cum misi_ contactu tenax lacus, **quaerensque invitum
premuntur** patria. Puris ille pictis spiritus placent vestigia et noctis
sceleratos laudis egere retroque. Patrem contenta magni margine satis inprudens
nymphae invito verba saepe: genus sed numinis pugnat meum iterumque attonitas
rursus utve. Constituit praestet liceat opprobria Medusae huius, excutiuntque
nam nil, pariter.

Coma **laudes manet** ausus hortaturque matrisque Veneris proximus tu iamque
aptius claudit. Tmolus tetigere iussos animumque quid poplite Hippotaden? Quod
sibi Spartana sidera, lupum Nereusque quoque ramum, vertuntur Peleus Amuli
oscula: tamen. Surgere Epidaurius movit crede soceri Euboicam quoque.

</br>
Unde stabant, acuta, percussit denique; hoc illic et herbis minimas parvum? Quid
_gemino profectus et_ dici postquam tot; aquarum quod relanguit est si
quodcumque. Ossaque protinus, quod somno est, repetit, hoc passu est. Qui devia;
respice humum vobis oscula, in Lotis nymphae.
