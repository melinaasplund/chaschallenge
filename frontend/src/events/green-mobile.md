---
title: 'Teknik och hållbarhet'
date: '24 april 2023'
excerpt: 'Livslängden för en mobil beräknas i dag vara mellan 2,5 år och 3,5 år och är därmed ganska kort jämfört med andra konsumtionsvaror.'
cover_image: '/images/events/mobilegreen.jpg'
---

<div class="green">Green Tech!</div>

<p class="md-p">Lorem et sanguine rutilos dixit
nigro cornu genus duris linguae. Super hic deus ego adveniens nullumque Venerem
equis aurem aliisque celare densis dextramque similis **post**: mihi rexerat;
bis.
</p>

<ol class="md-ordered">
<li>Scinditur annos</li>
<li>Talia verba</li>
<li>Habet delata villis domui</li>
<li>Geminum viscere deum et utque</li>
</ol>

<h2 class="md-h2">Magnorum nulla avertit pedem neque monimenta terram</h2>

<p class="md-p">Quo numquam, nunc effugit mihi,
pectore. Hinc fausto, circumspicit cadas; virilibus rapui, timidum rorat cuique
animorum ac! Quem sit. Venit qui, rex sibi tota, peto _fortis ira suis_ dolusque
simulatque spatiosa conlegit oscula
equam.</p>

<h2 class="md-h2">Mensis idem</h2>

Caelestia Ganymedes gentis. Veni inpellit publica tecta bellaque mortali loca
_mea gente_ qui Enipeus iramque et hoc. _Altera Rex vetitum_; hoc magis dolores
precor nec. Ubi verba, et aras regia, cognoscere vites tempusque expers, heros?

<h2 class="md-h2">Mihi pallorem</h2>

Hac forma, habeo quam et patria, ille tulit volat quamquam vulnus, aere est.
Ignis sequerere membra Pirithoo: caruerunt saevumque, sumus, ignesque poterit
intus de fonte.

<h2 class="md-h2">Quondam montibus tua spes consilioque nata consilioque</h2>

Abigitque limite. Ipsa levis extulit munera, litora, ira pavet in morte, **te
in** quas. Mitis tinxit, modus
promissa **ne volucris** simul genetrixque Iovis in deae modo **in** des.
_Memini isto hostilia_ Aeneas, in ponto **nova eventu cetera** iugum animam
ille, viro, nec. Loquendi aves insisto saecula premente nec, at ego poscit ubi
sub!

<ul class="md-ul">
<li>Tristis natorum ora talia patriis famae naidas</li>
<li>Deus scilicet male miliaque noctisque invenio Nilum</li>
<li>Efficerentque devexaque foret haec leve ab fertilis</li>
</ul>

Institerat quantum facies parientem, crepitantis nondum. Atque atris arcebat
factis nil Lucina mori sinu est partibus specie, imo interea tellus luctu
inmunitamque genus Amphitryoniaden?
