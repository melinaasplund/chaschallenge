import React from 'react'
import Link from 'next/link'

function Event({ event }) {
	return (
		<div className='p-4 rounded-lg shadow-xl flex flex-col mb-4'>
			<img src={event.frontmatter.cover_image} alt='' className='md-img' />
			<div className='mb-5 mt-4 text-sm bg-[#f4f5f7] py-1 px-3 rounded-md'>Datum: {event.frontmatter.date}</div>
			<h3 className='text-base font-semibold'>{event.frontmatter.title}</h3>
			<p className='text-sm'>{event.frontmatter.excerpt}</p>
			<Link href={`/eventPage/${event.slug}`}>
				<span className='mt-4 mb-4 inline-block bg-[#78BEC4] text-white font-medium py-1 px-3 rounded-md cursor-pointer text-base no-underline hover:scale-98'>
					Läs mer
				</span>
			</Link>
		</div>
	)
}

export default Event
