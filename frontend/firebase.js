// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getApp, getApps } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Firebase configuration
const firebaseConfig = {
	apiKey: 'AIzaSyDGeQiK9DhUDGYBBesiV7jwooOO60awh6E',
	authDomain: 'chas-challenge.firebaseapp.com',
	projectId: 'chas-challenge',
	storageBucket: 'chas-challenge.appspot.com',
	messagingSenderId: '917771304010',
	appId: '1:917771304010:web:848740651c72c78ebd7ab3',
}

// Initialize Firebase
export const app = !getApps().length ? initializeApp(firebaseConfig) : getApp()
export const auth = getAuth(app)
export const db = getFirestore(app)
