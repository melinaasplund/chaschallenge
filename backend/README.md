# Configurations for the webserver
The configurations for the webserver is automated with Ansible Playbooks.

To begin, edit the inventory.yml file.
- change the value of *ansible_user* to your username
- change the value of *ansible_ssh_private_key_file* to your ssh private key path

## configurations
After editing the inventory.yml file you can start running the playbooks with the following command *ansible-playbook -i inventory.yml <playbook.yml>*
> replace <playbook.yml> with the playbook of choice

- The playbook **setup.yml** updates and upgrades packages, opens the required ports and enables the firewall, and secures ssh
- The playbook **caddy_config.yml** installs and configures caddy
- The playbook **startApp.yml** pulls the application image from the container registry and runs it
